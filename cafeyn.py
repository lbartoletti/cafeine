#!/usr/bin/env python3

import argparse
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
import time
import requests
import img2pdf
import os
import re

# Setup argparse
parser = argparse.ArgumentParser(description='Télécharger des images et créer un PDF.')
parser.add_argument('--profile_path', help='Chemin vers le profil Firefox', required=True)
parser.add_argument('--url', help='URL à visiter', required=True)
parser.add_argument('--download_dir', help='Répertoire de téléchargement des images', required=True)

args = parser.parse_args()

download_dir = args.download_dir
os.makedirs(download_dir, exist_ok=True)



options = Options()
options.profile = args.profile_path
options.add_argument('--headless')
driver = webdriver.Firefox(options=options)

driver.get(args.url)
time.sleep(5)
html_content = driver.page_source
driver.quit()

soup = BeautifulSoup(html_content, 'lxml')

image_urls = [img['src'].replace('tn-', 'md-') for img in soup.find_all('img') if 'tn-' in img['src'] and img['src'].endswith('.jpeg')]

# Nom du journal
journal_name = ""
match = re.search(r"content_name:.*\"(.*)\"", html_content)

if match:
    journal_name = match.group(1)
    print("Texte extrait :", journal_name)
else:
    print("Aucun texte trouvé entre les guillemets.")
    with open(os.path.join(download_dir, "output.html"), "w") as f:
        f.write(html_content)
    exit()

# Numéro du journal 
journal_number = ""
div_content = soup.find('div', class_='or-topbar-date hidden md:block svelte-1rgcu63')
if div_content:
    journal_number = div_content.text.strip()
    print("Contenu extrait :", journal_number)
else:
    print("Aucun contenu trouvé pour la classe spécifiée.")

output_pdf_path = os.path.join(download_dir, journal_name, journal_number + '.pdf')

os.makedirs(os.path.join(download_dir, journal_name), exist_ok=True)

for url in image_urls:
    url = url.replace('tn', 'md')
    response = requests.get(url)
    if response.status_code == 200:
        file_name = url.split('/')[-1]
        file_path = os.path.join(download_dir, file_name)
        with open(file_path, 'wb') as f:
            f.write(response.content)
        print(f"Image téléchargée et sauvegardée : {file_path}")
    else:
        print(f"Échec du téléchargement de {url}")

def extract_number(filename):
    numbers = re.findall(r'\d+', filename)
    return int(numbers[0]) if numbers else 0

image_files = [f for f in os.listdir(download_dir) if f.endswith('.jpeg')]
image_files.sort(key=extract_number)

image_paths = [os.path.join(download_dir, f) for f in image_files]

with open(output_pdf_path, "wb") as f:
    f.write(img2pdf.convert(image_paths))

print(f"PDF créé avec succès : {output_pdf_path}")

# Nettoyer et supprimer les images téléchargées
for image_path in image_paths:
    os.remove(image_path)
    print(f"Image supprimée : {image_path}")

print("Nettoyage terminé.")